package org.minbox.chapter.springboot.constructor.binding.properties;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan
public class SpringbootConstructorBindingPropertiesApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootConstructorBindingPropertiesApplication.class, args);
    }

}
