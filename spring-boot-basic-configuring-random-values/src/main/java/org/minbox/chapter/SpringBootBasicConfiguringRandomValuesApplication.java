package org.minbox.chapter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootBasicConfiguringRandomValuesApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootBasicConfiguringRandomValuesApplication.class, args);
    }
}
